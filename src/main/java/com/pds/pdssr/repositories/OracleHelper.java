package com.pds.pdssr.repositories;

import oracle.jdbc.OracleConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;

@Component
public class OracleHelper {

    private final Logger log = LoggerFactory.getLogger(OracleHelper.class);

    private final DataSource dataSource;
    private boolean haveSessionInfo = false;

    private int sid;
    private int serial;
    private int spid;
    private int pid;

    @Autowired
    public OracleHelper(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public boolean isOracleConnection() {
        try (final Connection connection = this.dataSource.getConnection()) {
            return connection.isWrapperFor(OracleConnection.class);
        } catch (SQLException e) {
            log.error("Error when checking isOracleConnection: {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public OracleConnection getConnection() {
        if (isOracleConnection()) {
            try (final Connection connection = this.dataSource.getConnection()) {
                return connection.unwrap(OracleConnection.class);
            } catch (SQLException e) {
                log.error("Error when get connection from datasource");
                throw new RuntimeException(e);
            }
        } else {
            throw new java.lang.UnsupportedOperationException("Application not connected to Oracle Database");
        }
    }

    public void commit(final boolean asynchronous) {
        final String commitBatchNoWaitText = "commit work write batch nowait";
        try (final Connection connection = this.getConnection()) {
            if (!asynchronous) {
                this.getConnection().commit();
            } else {
                PreparedStatement ps = connection.prepareStatement(commitBatchNoWaitText);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            log.error("Cannot commit because: {}", e);
            throw new RuntimeException(e);
        }
    }

    public boolean traceOff() {
        boolean success = true;
        try (final Statement stmt = this.getConnection().createStatement()) {
            stmt.execute("alter session set timed_statistics = false");
            stmt.execute("alter session set sql_trace = false");
        } catch (final SQLException e) {
            if (e.getErrorCode() == 1031) {
                success = false;
                log.warn("no alter session permission 1031" + e.getMessage());
            } else if (e.getErrorCode() == -1031) {
                success = false;
                log.warn("no alter session -1031 " + e.getMessage());
            } else {
                log.error("Error when trace-off: {}", e);
                throw new RuntimeException(e);
            }
        }
        return success;
    }

    public boolean traceOn(final String fileId) throws SQLException {
        boolean success = true;
        try (final Statement stmt = this.getConnection().createStatement()) {
            stmt.execute("alter session set timed_statistics = true");
            stmt.execute("alter session set max_dump_file_size = unlimited");
            stmt.execute("alter session set sql_trace = true");
            stmt.execute("alter session set events '10046 trace name context forever, level 12'");
            setTraceFileIdentifier(fileId);
        } catch (final SQLException e) {
            if (e.getErrorCode() == 1031) {
                success = false;
                log.warn("no alter session permission 1031" + e.getMessage());
            } else if (e.getErrorCode() == -1031) {
                success = false;
                log.warn("no alter session -1031 " + e.getMessage());
            } else {
                log.error("Error when trace-on: {}", e);
                throw new RuntimeException(e);
            }
        }
        return success;
    }

    public void getSessionInfo() {
        if (!haveSessionInfo) {
            final String text = "select s.sid, s.serial#, p.spid, p.pid from v$session s, v$process p "
                    + " where s.audsid=userenv('sessionid') and p.addr = s.paddr";
            try (final Statement stmt = this.getConnection().createStatement()) {
                final ResultSet rset = stmt.executeQuery(text);
                rset.next();
                sid = rset.getInt("sid");
                serial = rset.getInt("serial#");
                spid = rset.getInt("spid");
                pid = rset.getInt("pid");
                haveSessionInfo = true;
                log.info("sid " + sid + " serial " + serial + " spid " + spid + " pid " + pid);
            } catch (final SQLException e) {
                log.error("Error when getSessionInfo() because: {}", e.getMessage());
            }
        }
    }

    // Fixme: Not clear what the purpose of this method. I think this need to parse parameter based on input, too, but
    // not sure about it.
    public void setAction() {
        String text = "{call dbms_application_info.set_action(?)}";
        try (CallableStatement sqlAction = this.getConnection().prepareCall(text)) {
            sqlAction.execute();
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void setModule(final String module) {
        String text = module;
        if (text.length() > 48) {
            final int end = text.length() - 1;
            final int begin = end - 48;
            text = module.substring(begin, end);
        }
        final String sqlString = "{call dbms_application_info.set_module(?,?)}";
        try (CallableStatement sqlModule = this.getConnection().prepareCall(sqlString)) {
            sqlModule.setString(1, text);
            sqlModule.setString(2, "");
            sqlModule.execute();
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void setClientInfo(final String info) {
        String text = info;
        if (text.length() > 32) {
            text = info.substring(0, 31);
        }

        final String sql = "{call dbms_application_info.set_client_info(:txt)}";
        try (CallableStatement sqlClientInfo = this.getConnection().prepareCall(sql)) {
            sqlClientInfo.setString(1, text);
            sqlClientInfo.executeUpdate();
        } catch (final SQLException sqe) {
            throw new RuntimeException(sqe);
        }
    }

    public void setClientIdentifier(final String info) {
        String text = info;
        if (text.length() > 32) {
            text = info.substring(0, 31);
        }
        final String sql = "{call dbms_session.set_identifier(:txt)}";
        try (CallableStatement sqlClientInfo = this.getConnection().prepareCall(sql)) {
            sqlClientInfo.setString(1, text);
            sqlClientInfo.executeUpdate();
        } catch (final SQLException sqe) {
            throw new RuntimeException(sqe);
        }
    }


    // --

    public int getSid() {
        return sid;
    }

    public int getSerial() {
        return serial;
    }

    public int getSpid() {
        return spid;
    }

    public int getPid() {
        return pid;
    }


    // --

    private void setTraceFileIdentifier(final String id) {
        final String text = "alter session set tracefile_identifier = '" + id + "'";
        try (final PreparedStatement stmt = this.getConnection().prepareStatement(text)) {
            stmt.executeUpdate();
        } catch (final SQLException s) {
            log.error("Error when setTraceFileIdentifier with id: {}, because: {}", id, s.getMessage());
        }
    }
}
